
--
-- Database: `lp2_danton`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `dadosbusca`
--

CREATE TABLE `dadosbusca` (
  `id` int(11) NOT NULL,
  `busca` varchar(50) DEFAULT NULL,
  `categoria` varchar(13) NOT NULL,
  `resultado` text,
  `last_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `dadosbusca`
--

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dadosbusca`
--
ALTER TABLE `dadosbusca`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dadosbusca`
--
ALTER TABLE `dadosbusca`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

