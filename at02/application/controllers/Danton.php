<?php

class Danton extends CI_Controller{
    public function index(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->view('index_view');
        $this->load->view('common/footer');
    }

    public function feed(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');

        $this->load->model('FeedModel', 'fm');
        $data['newsfeed'] = $this->fm->getNewsFeed();
        $this->load->view('feed_view', $data);

        $this->load->view('common/footer');
    }
    
    public function busca(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');

        $this->load->model('FeedModel', 'fm');
        $data['newssearch'] = $this->fm->getNewsSearch();
        $this->load->view('busca_view', $data);

        $this->load->view('common/footer');
    }

    public function relatorio(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');

        $this->load->model('FeedModel', 'fm');
        $data['lista'] = $this->fm->lista();
        $this->load->view('table_view', $data);

        $this->load->view('common/footer');
    }
    
    public function api(){
        $this->load->view('common/header');
        $this->load->view('common/navbar');
        $this->load->view('api_view');
        $this->load->view('common/footer');
    }
}


?>