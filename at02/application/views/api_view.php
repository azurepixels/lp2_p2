<div class="container col-md-10">
    <h4 class="mt-4">Quais são as principais características da API e suas finalidades?</h4>
    <p>É uma API de Feed de Notícias, que utiliza uma chave API e retorna dados em JSON.</p>
    <br>
    <h4>O quê é possível criar com o uso desta API?</h4>
    <p>É possivel criar um Feed de Notícias, podendo ordená-los por data, popularidade, consultar por categoria, fonte, país, e até mesmo fazer uma busca utilizando palavras-chave em uma pesquisa.</p>
    <br>
    <h4>Quais são as restrições para o uso da API? Tem custo? Tem boa documentação? E outras...</h4>
    <p>É gratuita e open-source para todo desenvolvimento não-comercial, leve e rápida de utilizar com uma documentação simples e objetiva, apesar de estar completamente em inglês.</p>
    <br>
    <h4>Quais foram os passos necessários para a implementação da sua aplicação com o uso desta API?</h4>
    <p>Foi necessário apeans a criação de uma conta no site oficial para obter a chave API. Após isso, basta somente incluir uma das endpoints disponíveis no seu código e converter o arquivo JSON para um vetor.</p>
    <br>
    <a href="https://newsapi.org/"><p>Link para o site oficial.</p></a>
</div>