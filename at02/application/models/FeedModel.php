<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH.'libraries/News.php';


class FeedModel extends CI_Model{

    public function getNewsFeed(){

        $html = $this->getForm('Feed de Notícias');

        $news_url = 'https://newsapi.org/v2/top-headlines?country=br&apiKey=a60705f0b3ce49f6971f880fa8d9884e';
        $html .= $this->showNews($news_url);
        return $html;
    }



    public function getNewsSearch(){
        
        $busca = $this->input->get('busca');
        $busca1 = urlencode($busca);
        $categoria = $this->input->get('categoria');

        if(!empty($busca1) || !empty($categoria)){
            $news_url = 'https://newsapi.org/v2/top-headlines?q='.$busca1.'&category='.$categoria.'&country=br&apiKey=a60705f0b3ce49f6971f880fa8d9884e';
            $html = $this->showNews($news_url);
        }

        $dados = $this->getArray($news_url);

        if(empty($dados)){
            $html = $this->getForm('Dados Inválidos: Tente novamente');
        }
        else{
            $this->setDados($dados, $busca, $categoria);
        }

        return $html;
    }



    public function setDados($dados, $busca, $categoria){
        $news = new News();
        $news->setBusca($this->input->get('busca'));
        $news->setCategoria($this->input->get('categoria'));
        $news->setResultado($dados);
        $news->save();
    }



    public function getArray($news_url){
        $news_json = file_get_contents($news_url);
        $news_array = json_decode($news_json, true);
        $dados = $news_array['articles'];
        return $dados;
    }


    
    public function showNews($news_url){
        $dados = $this->getArray($news_url);

        $html = '<div class="row">
                    <div class="card-deck mt-4">';

        foreach ($dados as $noticia) {
            $news = new News($noticia);
            $html .= $news->getHTML();
        }

        $html .= '</div>
                </div>';
        
        return $html;
    }



    public function getForm($title){

        $html = '<div class="row">
                    <div class="col-md-12">
                        <h3 class="text-center mt-4">'.$title.'</h3>
                        <div class="card mt-4 mb-4">
                            <div class="card-body">
                                <form action="'.base_url('danton/busca').'" method="GET">
                                    <div class="text-center">   
                                        <p>
                                            <input type="text" id="busca" name="busca" placeholder="Pesquisar...">
                                                <span class="ml-3">Categoria: <select name="categoria" class="mt-2">
                                                    <option value="" disabled>Categoria</option>
                                                    <option value="general" selected>Geral</option>
                                                    <option value="science">Ciência</option>
                                                    <option value="entertainment">Entretenimento</option>
                                                    <option value="sports">Esportes</option>
                                                    <option value="business">Negócios</option>
                                                    <option value="health">Saúde</option>
                                                    <option value="technology">Tecnologia</option>
                                                </select>
                                                </span>
                                            <button class="btn btn-sm btn-elegant ml-3" type="submit"><i class="fas fa-search"></i></button>
                                        </p>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>';

        return $html;
    }



    public function lista(){
        $html = '';
        $news = new News();
        $data = $news->getAll();
        foreach($data as $row){
            $html .= '<tr>';
            $html .= '<td>'.$row['busca'].'</td>';
            $html .= '<td>'.$row['categoria'].'</td>';
            $html .= '<td>'.$row['resultado'].'</td>';
            $html .= '<td>'.$row['last_modified'].'</td>';
            $html .= '</tr>';
        }
        return $html;
    }

}


?>