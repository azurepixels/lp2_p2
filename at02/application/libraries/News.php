<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News{

    private $conteudo;

    private $busca;
    private $categoria;
    private $resultado;

    private $db;

    function __construct($noticia=null){
        $this->conteudo = $noticia;
        $ci = &get_instance();
        $this->db = $ci->db;
    }

    public function getHTML(){
        $html = '<div class="col-md-4">
                    <div class="card mb-4">
                        <img class="card-img-top" src="'.$this->conteudo['urlToImage'].'">
                        <div class="card-body">
                              <h4 class="card-title">'.$this->conteudo['title'].'</h4>
                              <p class="card-text">'.$this->conteudo['description'].'</p>
                               <div class="text-center">
                                  <a href="'.$this->conteudo['url'].'" class="btn btn-elegant">Ver Notícia</a>
                               </div>
                          </div>
                          <div class="card-footer text-muted text-center">
                               Fonte: '.$this->conteudo['source']['name'].'
                           </div>
                    </div>
                  </div>';
        
        return $html;
    }

    public function setBusca($busca){
        $this->busca = $busca;
    }

    public function setCategoria($categoria){
        $this->categoria = $categoria;
    }

    public function setResultado($resultado){
        $dados = '';
        $remove = "'";
        foreach($resultado as $row){
            $dados .= '<p><span class="font-weight-bold">Imagem:</span> '.$row['urlToImage'].'</p>';
            $dados .= '<p><span class="font-weight-bold">Título:</span> '.str_replace("'", "", $row['title']).'</p>';
            $dados .= '<p><span class="font-weight-bold">Descrição:</span> '.str_replace("`", "", str_replace("'", "", $row['description'])).'</p>';
            $dados .= '<p><span class="font-weight-bold">URL:</span> '.$row['url'].'</p>';
            $dados .= '<p><span class="font-weight-bold">Fonte:</span> '.$row['source']['name'].'</p>';
            $dados .= '<hr>';
        }
        $this->resultado = $dados;
    }

    public function save(){
        $sql = "INSERT INTO dadosbusca (busca, categoria, resultado)
        VALUES ('$this->busca', '$this->categoria', '$this->resultado')"; 
        $this->db->query($sql);
    }


    public function getALL(){
        $sql = "SELECT * FROM dadosbusca";
        $res = $this->db->query($sql);
        return $res->result_array();
    }


}


?>