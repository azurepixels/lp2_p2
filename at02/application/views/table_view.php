<div class="container col-md-10">
    <div class="row">
        <div class="col-md-12">
            <table class="table table-bordered table-sm table-responsive-lg mt-4">
                <thead class="black white-text">
                    <th>Busca</th>
                    <th>Categoria</th>
                    <th>Resultado</th>
                    <th>Horário</th>
                </thead>

                <tbody>
                    <?= $lista ?>
                </tbody>

            </table>
        </div>
    </div>
</div>