<!--Navbar-->
<nav class="navbar navbar-expand-lg sticky-top navbar-dark elegant-color">

  <!-- Navbar brand -->
  <a class="navbar-brand" href="<?= base_url('danton') ?>">
    <img src="<?= base_url('/assets/img/aluno/navbar.jpg') ?>" height="50" alt="mdb logo">
  </a>

  <!-- Collapse button -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <!-- Collapsible content -->
  <div class="collapse navbar-collapse" id="basicExampleNav">

    <!-- Links -->
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('danton') ?>" >Home</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('danton/feed') ?>">Feed de Notícias</a>
      </li>

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
          aria-haspopup="true" aria-expanded="false">Categorias</a>
        <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="<?= base_url('danton/busca?categoria=science') ?>">Ciência</a>
          <a class="dropdown-item" href="<?= base_url('danton/busca?categoria=entertainment') ?>">Entretenimento</a>
          <a class="dropdown-item" href="<?= base_url('danton/busca?categoria=sports') ?>">Esportes</a>
          <a class="dropdown-item" href="<?= base_url('danton/busca?categoria=business') ?>">Negócios</a>
          <a class="dropdown-item" href="<?= base_url('danton/busca?categoria=health') ?>">Saúde</a>
          <a class="dropdown-item" href="<?= base_url('danton/busca?categoria=technology') ?>">Tecnologia</a>
        </div>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('danton/relatorio') ?>">Relatório de dados</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="<?= base_url('danton/api') ?>">Sobre a API</a>
      </li>

    </ul>

  </div>
  <!-- Collapsible content -->

</nav>
<!--/.Navbar-->